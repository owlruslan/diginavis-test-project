import React from "react";
import {Box, Button, Form, FormField, Heading, Layer, TextInput} from 'grommet';
import './CreatePlan.scss';
import {useForm} from 'react-hook-form';
import {useDispatch} from 'react-redux';
import {AddFlightPlan} from '../../models/add-flight-plan.model';
import {addFlight} from '../../store/flights/flights.actions';

type Inputs = {
  description: string;
};

type Props = {
  onCloseCreate: any
}

export const CreatePlan = ({onCloseCreate}: Props) => {
  const { register, handleSubmit } = useForm<Inputs>();

  const [show, setShow] = React.useState(true);

  const dispatch = useDispatch();

  const onClose = () => {
    setShow(false);
    onCloseCreate(false);
  }

  const onSubmit = (data: Inputs) => {
    const plan = new AddFlightPlan(data.description)
    dispatch(addFlight(plan))
  }

  return (
    <Box>
      {show && (
        <Layer onClickOutside={() => show} onEsc={onClose}>
          <Heading margin="none">Create Plan</Heading>
          <Form onSubmit={handleSubmit(onSubmit)}>
            <FormField name="description" label="Description">
              <TextInput id="text-input-id" name="description" ref={register({ required: true })}/>
            </FormField>
            <Box direction="row" gap="medium">
              <Button type="submit" primary label="Submit" />
              <Button onClick={onClose} label="Close"/>
            </Box>
          </Form>
        </Layer>
      )}
    </Box>
  )
}
