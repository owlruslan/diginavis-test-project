import React, {useEffect} from "react";
import {Box, Button, CheckBox, Form, FormField, Header, Heading, Layer, List, TextInput} from 'grommet';
import './AddDroneToPlan.scss';
import {useForm} from 'react-hook-form';
import {useDispatch, useSelector} from 'react-redux';
import {RootStoreState} from '../../store/root';
import {FlightPlan} from '../../models/flight-plan.model';
import {loadDrones} from '../../store/drones/drones.actions';
import {addDroneToPlan} from '../../store/flights/flights.actions';

type Props = {
  id: number,
  onCloseDroneToPlan: any
}

export const AddDroneToPlan = ({id, onCloseDroneToPlan}: Props) => {
  const [show, setShow] = React.useState(true);

  const dispatch = useDispatch();

  React.useEffect(() => {
    dispatch(loadDrones())
  }, [])

  const selected: any[] | undefined = useSelector((state: RootStoreState.State) => {
    const drone = state.flights.entities.find(flight => flight.id === id)
    if (drone) {
      return drone.drones;
    }
  });

  const unselected: any[] | undefined = useSelector((state: RootStoreState.State) => {
    const allDrones = state.drones.entities.map(drone => drone.id);
    const flight = state.flights.entities.find(flight => flight.id === id);
    if (flight) {
      const allFlightDrones = flight.drones;
      return allDrones.filter(drone => !allFlightDrones.includes(drone));
    }
  });

  const onClose = () => {
    setShow(false);
    onCloseDroneToPlan(false);
  }

  const selectDrone = (droneId: number) => {
    dispatch(addDroneToPlan(id, droneId));
  }

  return (
    <Box>
      {show && (
        <Layer onClickOutside={() => show} onEsc={onClose}>
          <Heading margin="none">Add Drone To Plan</Heading>
          <Box direction="row" gap="medium" pad="medium" justify={'between'}>
            <Heading level={5} size="small">
              Selected
            </Heading>
            <Heading level={5} size="small">
              Unselected
            </Heading>
          </Box>
          <Box direction="row" gap="medium" pad="medium" justify={'between'}>
            <Box direction="column" gap="medium" pad="medium" justify={'between'}>
              {selected ? selected.map((id) => <span className="row" key={id}><span>{id}</span></span>) : null}
            </Box>
            <Box direction="column" gap="medium" pad="medium" justify={'between'}>
              {unselected ? unselected.map((id) => <span className="row" key={id} onClick={() => selectDrone(id)}><span>{id}</span></span>) : null}
            </Box>
          </Box>
        </Layer>
      )}
    </Box>
  )
}
