import React from "react";
import {Box, Button, CheckBox, Form, FormField, Heading, Layer, TextInput} from 'grommet';
import './CreateDrone.scss';
import {useForm} from 'react-hook-form';
import {useDispatch} from 'react-redux';
import {addDrone} from '../../store/drones/drones.actions';
import {AddDrone} from '../../models/add-drone.model';

type Inputs = {
  name: string,
  isFavorite: boolean,
};

type Props = {
  onCloseCreateDrone: any
}

export const CreateDrone = ({onCloseCreateDrone}: Props) => {
  const { register, handleSubmit } = useForm<Inputs>();

  const [show, setShow] = React.useState(true);

  const dispatch = useDispatch();

  const onClose = () => {
    setShow(false);
    onCloseCreateDrone(false);
  }

  const onSubmit = (data: Inputs) => {
    if (!data.isFavorite) {
      data.isFavorite = false;
    }
    const drone = new AddDrone(data.name, data.isFavorite)
    dispatch(addDrone(drone))
  }

  return (
    <Box>
      {show && (
        <Layer onClickOutside={() => show} onEsc={onClose}>
          <Heading margin="none">Create Drone</Heading>
          <Form onSubmit={handleSubmit(onSubmit)}>
            <FormField name="name" label="Name">
              <TextInput id="text-input-id" name="name" ref={register({ required: true })} />
            </FormField>
            <FormField name="isFavorite" label="isFavorite">
              <CheckBox id="check-box" name="isFavorite" label="isFavorite" ref={register} />
            </FormField>
            <Box direction="row" gap="medium">
              <Button type="submit" primary label="Submit" />
              <Button onClick={onClose} label="Close"/>
            </Box>
          </Form>
        </Layer>
      )}
    </Box>
  )
}
