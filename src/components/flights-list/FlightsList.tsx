import React, {useState} from "react";
import {Button, Heading, RadioButtonGroup, Table, TableBody, TableCell, TableHeader, TableRow, Text} from 'grommet';
import './FlightsList.scss';
import {useDispatch, useSelector} from 'react-redux';
import {loadFlights, removeFlight, setStatus} from '../../store/flights/flights.actions';
import {RootStoreState} from '../../store/root';
import {FlightStatus} from '../../models/flight-status.model';
import {CreatePlan} from '../create-plan/CreatePlan';
import {AddDroneToPlan} from '../add-drone-to-plan/AddDroneToPlan';

export const columns = [
  {
    property: 'id',
    label: 'ID',
  },
  {
    property: 'description',
    label: 'Description',
  },
  {
    property: 'status',
    label: 'Status',
  },
  {
    property: 'drones',
    label: 'Drones',
  },
];

export const FlightsList = (props: any) => {
  const [page, setPage] = useState(0);

  const dispatch = useDispatch();

  const [showCreate, setShowCreate] = useState(false)
  const [showAddDrone, setShowAddDrone] = useState(false)
  const [currentPlanId, setCurrentPlanId] = useState<number>(0)

  const plans = useSelector((state: RootStoreState.State) => {
    return state.flights.entities
  });

  React.useEffect(() => {
    dispatch(loadFlights());
  }, []);

  React.useEffect(() => {
    setShowCreate(props.create);
  }, [props]);

  const setFlightStatus = (event: any, id: string) => {
    dispatch(setStatus(+id, event.target.value as FlightStatus));
  }

  const addDroneToPlan = (id: number) => {
    setCurrentPlanId(id)
    setShowAddDrone(true);
  }

  const onCloseDroneToPlan = (value: boolean) => {
    setShowAddDrone(value);
  }

  const onCloseCreate = (value: boolean) => {
    setShowCreate(value);
  }

  return <>
    <Heading margin="none">Flights List</Heading>
    <Button primary label="Create Plan" className="button" onClick={() => setShowCreate(true)}/>
    <Table>
      <TableHeader>
        <TableRow>
          {columns.map(c => (
            <TableCell key={c.property} scope="col">
              <Text>{c.label}</Text>
            </TableCell>
          ))}
          <TableCell scope="col">
            <Text>Controls</Text>
          </TableCell>
        </TableRow>
      </TableHeader>
      <TableBody>
        {plans.length > 0 ? plans.map((plan: any) => (
          <TableRow key={plan.id}>
            {columns.map(column => (
              <TableCell key={column.property} scope="row" alignSelf="start">
                {
                  column.property === 'drones' ?
                    <Text className="drones" onClick={() => addDroneToPlan(plan.id)}>{plan[column.property].length}</Text>
                    : <Text>{plan[column.property]}</Text>
                }
              </TableCell>
            ))}
            <TableCell scope="row" alignSelf="start">
              <div className="manage">
                <Button primary label="Delete" className="button" onClick={() => dispatch(removeFlight(plan.id))}/>
                <RadioButtonGroup
                  name="radio"
                  options={[
                    {label: "initial", value: "initial"},
                    {label: "started", value: "started"},
                  ]}
                  value={plan.status}
                  onChange={(event: any) => setFlightStatus(event, plan.id.toString())}
                />
              </div>
            </TableCell>
          </TableRow>
        )) : null}
      </TableBody>
    </Table>
    <div className="pagination">
      {page > 0 ? <Button primary label="Prev" className="button" onClick={() => {
        let newPage = page - 1;
        setPage(newPage)
        return dispatch(loadFlights(newPage))
      }}/> : null}
      <Button primary label="Next" className="button" onClick={() => {
        let newPage = page + 1;
        setPage(newPage)
        return dispatch(loadFlights(newPage))
      }}/>
    </div>
    {showCreate ? <CreatePlan onCloseCreate={onCloseCreate}/> : null}
    {showAddDrone ? <AddDroneToPlan id={currentPlanId} onCloseDroneToPlan={onCloseDroneToPlan}></AddDroneToPlan> : null}
  </>
}
