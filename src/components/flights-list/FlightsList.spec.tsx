// @ts-ignore
import {LOAD_FLIGHTS, loadFlights} from '../../store/flights/flights.actions';
import configureStore from 'redux-mock-store' //ES6 modules
import React from 'react';
import {FlightsList} from './FlightsList';

const middlewares: any[] = []
const mockStore = configureStore(middlewares)


describe('FlightsList', () => {
  let useEffect: any;

  const initialState = {}
  const store = mockStore(initialState)

  const mockUseEffect = () => {
    useEffect.mockImplementationOnce((f: Function) => f());
  };

  beforeEach(() => {
    useEffect = jest.spyOn(React, "useEffect");
  });

  it('should dispatch action `LOAD_FLIGHTS `', () => {
    mockUseEffect();
    // Initialize mockstore with empty state
    const initialState = {}
    const store = mockStore(initialState)

    // Dispatch the action
    store.dispatch(loadFlights())

    // Test if your store dispatched the expected actions
    const actions = store.getActions()
    const expectedPayload = { type: LOAD_FLIGHTS }
    expect(actions).toEqual([expectedPayload])
  })
})
