import React, {useState} from "react";
import {Button, Heading, Table, TableBody, TableCell, TableHeader, TableRow, Text} from 'grommet';
import './DronesList.scss';
import {useDispatch, useSelector} from 'react-redux';
import {getFavoriteDrones, loadDrones, removeDrone, setFavoriteDrone} from '../../store/drones/drones.actions';
import {RootStoreState} from '../../store/root';
import {CreateDrone} from '../create-drone/CreateDrone';

export const columns = [
  {
    property: 'id',
    label: 'ID',
  },
  {
    property: 'name',
    label: 'Name',
  },
  {
    property: 'isFavorite',
    label: 'Favorite',
  },
];

export const DronesList = (props: any) => {
  const dispatch = useDispatch()

  const [showCreateDrone, setShowCreateDrone] = useState(false)

  const drones = useSelector((state: RootStoreState.State) => {
    return state.drones.entities
  });

  React.useEffect(() => {
    dispatch(loadDrones());
  }, []);

  React.useEffect(() => {
    if (props.favorites) {
      setIsFavorite(true);
      dispatch(getFavoriteDrones());
    } else {
      setIsFavorite(false);
      dispatch(loadDrones());
    }
    setShowCreateDrone(props.create);
  }, [props]);

  const [isFavorites, setIsFavorite] = useState(false);

  function onFavoriteHandler() {
    if (isFavorites) {
      setIsFavorite(false);
      dispatch(loadDrones());
    } else {
      setIsFavorite(true);
      dispatch(getFavoriteDrones());
    }
  }

  const onCloseCreateDrone = (value: boolean) => {
    setShowCreateDrone(value);
  }

  return <>
    <Heading margin="none">Drones List</Heading>
    <div className="buttons">
      <div className="button-wrapper">
        <Button primary label="Create Drone" className="button" onClick={() => setShowCreateDrone(true)}/>
      </div>
      <div className="button-wrapper">
        {isFavorites ?
          <Button primary label="Show All" className="button" onClick={onFavoriteHandler}/>
          : <Button primary label="Show Favorite" className="button" onClick={onFavoriteHandler}/>
        }
      </div>
    </div>
    <Table>
      <TableHeader>
        <TableRow>
          {columns.map(c => (
            <TableCell key={c.property} scope="col">
              <Text>{c.label}</Text>
            </TableCell>
          ))}
          <TableCell scope="col">
            <Text>Controls</Text>
          </TableCell>
        </TableRow>
      </TableHeader>
      <TableBody>
        {drones.length > 0 ? drones.map((drone: any) => (
          <TableRow key={drone.id}>
            {columns.map(column => (
              <TableCell key={column.property} scope="row" alignSelf="start">
                <Text>{drone[column.property].toString()}</Text>
              </TableCell>
            ))}
            <TableCell scope="row" alignSelf="start">
              <div className="manage">
                <div className="button-wrapper">
                  {
                    drone.isFavorite ? <Button primary label="Add to Favorite" className="button"
                                               onClick={() => dispatch(setFavoriteDrone(drone.id))}/>
                      : <Button primary label="Remove Favorite" size={'small'} className="button"
                                onClick={() => dispatch(setFavoriteDrone(drone.id))}/>
                  }
                </div>
                <div className="button-wrapper">
                  <Button primary label="Delete Drone" className="button"
                          onClick={() => dispatch(removeDrone(drone.id))}/>
                </div>
              </div>
            </TableCell>
          </TableRow>
        )) : null}
      </TableBody>
    </Table>
    {showCreateDrone ? <CreateDrone onCloseCreateDrone={onCloseCreateDrone}/> : null}
  </>
}
