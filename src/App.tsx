import React from 'react';
import {BrowserRouter as Router, NavLink, Route, Switch} from "react-router-dom";
import './App.scss';
import {Box, Grommet, Header, Main} from 'grommet';
import {FlightsList} from './components/flights-list/FlightsList';
import {DronesList} from './components/drones-list/DronesList';

const App: React.FC = () => {
  const theme = {
    global: {
      font: {
        family: 'Helvetica',
        size: '18px',
        height: '20px',
      },
    },
  };

  return (
    <Grommet theme={theme}>
      <Router>
        <Header background="brand">
          <Box direction="row" gap="medium" pad="medium">
            <NavLink exact to="/" activeClassName="active">
              Flights List
            </NavLink>
            <NavLink to="/create-plan" activeClassName="active">
              Create Flight Plan
            </NavLink>
            <NavLink to="/drones" activeClassName="active">
              Drones List
            </NavLink>
            <NavLink to="/create-drone" activeClassName="active">
              Create Drone
            </NavLink>
            <NavLink to="/favorites" activeClassName="active">
              Favorite drones
            </NavLink>
          </Box>
        </Header>
        <Main pad="large">
          <Switch>
            <Route path="/create-plan" render={(props) => <FlightsList create={true} {...props} />}/>
            <Route path="/drones">
              <DronesList/>
            </Route>
            <Route path="/create-drone" render={(props) => <DronesList create={true} {...props} />}/>
            <Route path="/favorites" render={(props) => <DronesList favorites={true} {...props} />}/>
            <Route path="/">
              <FlightsList/>
            </Route>
          </Switch>
        </Main>
      </Router>
    </Grommet>
  );
};

export default App;
