export interface Drone {
  id: number;
  name: string;
  isFavorite: boolean;
}
