import {FlightStatus} from './flight-status.model';

export class AddFlightPlan {
  description: string;
  status: FlightStatus;
  drones: number[]

  constructor(description: string) {
    this.description = description;
    this.status = FlightStatus.INITIAL;
    this.drones = [];
  }
}
