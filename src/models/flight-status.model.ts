export enum FlightStatus {
  INITIAL = 'initial',
  STARTED = 'started'
}
