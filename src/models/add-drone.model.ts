export class AddDrone {
  name: string;
  isFavorite: boolean;

  constructor(name: string, isFavorite: boolean) {
    this.name = name;
    this.isFavorite = isFavorite
  }
}
