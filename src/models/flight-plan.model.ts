export interface FlightPlan {
  id: number;
  description: string;
  status: string;
  drones: number[]
}
