import {Drone} from '../../models/drone.model';

export interface State {
  entities: Drone[]
}

export const initialState: State = {
  entities: [],
}
