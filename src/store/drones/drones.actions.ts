import {Drone} from '../../models/drone.model';
import {AddDrone} from '../../models/add-drone.model';

export const LOAD_DRONES = '[DRONES] LOAD DRONES'
export const LOAD_DRONES_SUCCESS = '[DRONES] LOAD DRONES SUCCESS'
export const LOAD_DRONES_FAILURE = '[DRONES] LOAD DRONES FAILURE'

export const ADD_DRONE = '[DRONES] ADD DRONE'

export const REMOVE_DRONE = '[DRONES] REMOVE DRONE'

export const SET_FAVORITE_DRONE = '[DRONES] SET_FAVORITE DRONE'

export const GET_FAVORITE_DRONES = '[DRONES] GET_FAVORITE DRONES'
export const GET_FAVORITE_DRONES_SUCCESS = '[DRONES] GET_FAVORITE DRONES SUCCESS'
export const GET_FAVORITE_DRONES_FAILURE = '[DRONES] GET_FAVORITE DRONES FAILURE'

export function loadDrones() {
  return {
    type: LOAD_DRONES,
  }
}

export function loadDronesSuccess(drones: Drone[]) {
  return {
    type: LOAD_DRONES_SUCCESS,
    payload: drones
  }
}

export function loadDronesFailure(e: any) {
  return {
    type: LOAD_DRONES_FAILURE,
    payload: e
  }
}

export function addDrone(drone: AddDrone) {
  return {
    type: ADD_DRONE,
    payload: drone
  }
}


export function removeDrone(id: string) {
  return {
    type: REMOVE_DRONE,
    payload: id
  }
}


export function setFavoriteDrone(id: string) {
  return {
    type: SET_FAVORITE_DRONE,
    payload: id
  }
}


export function getFavoriteDrones() {
  return {
    type: GET_FAVORITE_DRONES,
  }
}

export function getFavoriteDronesSuccess(drones: Drone[]) {
  return {
    type: GET_FAVORITE_DRONES_SUCCESS,
    payload: drones
  }
}

export function getFavoriteDronesFailure(e: any) {
  return {
    type: GET_FAVORITE_DRONES_FAILURE,
    payload: e
  }
}

