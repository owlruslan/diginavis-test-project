import {
  ADD_DRONE,
  GET_FAVORITE_DRONES,
  GET_FAVORITE_DRONES_FAILURE,
  GET_FAVORITE_DRONES_SUCCESS,
  LOAD_DRONES,
  LOAD_DRONES_FAILURE,
  LOAD_DRONES_SUCCESS,
  REMOVE_DRONE,
  SET_FAVORITE_DRONE
} from './drones.actions';
import {initialState} from './drones.state';
import {AddDrone} from '../../models/add-drone.model';

export const dronesReducer = (state = initialState, action: any) => {
  switch (action.type) {
    case LOAD_DRONES:
      return state;

    case LOAD_DRONES_SUCCESS:
      state.entities = action.payload

      return {
        ...state,
      };

    case LOAD_DRONES_FAILURE:
      return state;

    case ADD_DRONE:
      const drone: AddDrone = action.payload;
      const newDrone = {
        id: state.entities[state.entities.length - 1].id + 1,
        ...drone
      };

      return {
        ...state,
        entities: [...state.entities, newDrone]
      };

    case REMOVE_DRONE:
      return {
        ...state,
        entities: state.entities.filter(drone => drone.id !== action.payload)
      };

    case SET_FAVORITE_DRONE:
      return {
        ...state,
        entities: state.entities.map(drone => drone.id === action.payload ?
          { ...drone, isFavorite: !drone.isFavorite } :
          drone
        )
      };

    case GET_FAVORITE_DRONES:
      return state;

    case GET_FAVORITE_DRONES_SUCCESS:
      state.entities = action.payload

      return {
        ...state,
      };

    case GET_FAVORITE_DRONES_FAILURE:
      return state;

    default:
      return state;
  }
}
