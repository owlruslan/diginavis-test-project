import {call, put, takeEvery} from 'redux-saga/effects';
import {
  GET_FAVORITE_DRONES,
  getFavoriteDronesFailure,
  getFavoriteDronesSuccess,
  LOAD_DRONES,
  loadDronesFailure,
  loadDronesSuccess
} from './drones.actions';

export function* loadDronesWatcher() {
  yield takeEvery(LOAD_DRONES, loadDronesWorker)
}

export function* loadFavoriteDronesWatcher() {
  yield takeEvery(GET_FAVORITE_DRONES, loadFavoriteDronesWorker)
}

function* loadDronesWorker() {
  try {
    const user = yield call(fetchDrones);
    yield put(loadDronesSuccess(user))
  } catch (e) {
    yield put(loadDronesFailure(e))
  }
}

function* loadFavoriteDronesWorker() {
  try {
    const user = yield call(fetchFavoriteDrones);
    yield put(getFavoriteDronesSuccess(user))
  } catch (e) {
    yield put(getFavoriteDronesFailure(e))
  }
}

async function fetchDrones() {
  const response = await fetch('/drones', {method: 'GET'})
    .then((res: Response) => res.json())

  return await response;
}

async function fetchFavoriteDrones() {
  const response = await fetch('/drones?isFavorite=true', {method: 'GET'})
    .then((res: Response) => res.json())

  return await response;
}

export const dronesSagas = [
  loadDronesWatcher,
  loadFavoriteDronesWatcher
];
