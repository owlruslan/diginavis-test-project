import {call, put, takeEvery} from 'redux-saga/effects';
import {LOAD_FLIGHTS, loadFlightsFailure, loadFlightsSuccess} from './flights.actions';

export function* loadFlightsWatcher() {
  const action = yield takeEvery(LOAD_FLIGHTS, loadFlightsWorker)
}

function* loadFlightsWorker(action: any) {
  try {
    const user = yield call(fetchFlights, action);
    yield put(loadFlightsSuccess(user))
  } catch (e) {
    yield put(loadFlightsFailure(e))
  }
}

async function fetchFlights(action: any) {
  let response;
  if (action.payload > 0) {
    const page = action.payload + 1;
    response = await fetch(`/flightPlans?_page=${page}&_limit=3`, {method: 'GET'})
      .then((res: Response) => res.json())
  } else {
    response = await fetch('/flightPlans?_page=1&_limit=3', {method: 'GET'})
      .then((res: Response) => res.json())
  }

  return await response;
}

export const flightsSagas = [loadFlightsWatcher];
