import {
  ADD_DRONE_TO_PLAN,
  ADD_FLIGHT,
  LOAD_FLIGHTS,
  LOAD_FLIGHTS_FAILURE,
  LOAD_FLIGHTS_SUCCESS,
  REMOVE_FLIGHT,
  SET_STATUS
} from './flights.actions';
import {initialState} from './flights.state';
import {AddFlightPlan} from '../../models/add-flight-plan.model';

export const flightsReducer = (state = initialState, action: any) => {
  switch (action.type) {
    case LOAD_FLIGHTS:
      return {
        ...state,
      };

    case LOAD_FLIGHTS_SUCCESS:
      state.entities = action.payload

      return {
        ...state,
      };

    case LOAD_FLIGHTS_FAILURE:
      return {
        ...state,
      };

    case ADD_FLIGHT:
      const plan: AddFlightPlan = action.payload;
      const newFlightPlan = {
        id: state.entities[state.entities.length - 1].id + 1,
        ...plan
      };

      return {
        ...state,
        entities: [...state.entities, newFlightPlan]
      };

    case REMOVE_FLIGHT:
      return {
        ...state,
        entities: state.entities.filter(flight => flight.id !== action.payload)
      };

    case SET_STATUS:
      return {
        ...state,
        entities: state.entities.map(flight => flight.id === action.payload.id ?
          { ...flight, status: action.payload.status } :
          flight
        )
      };

    case ADD_DRONE_TO_PLAN:
      return {
        ...state,
        entities: state.entities.map(flight => flight.id === action.payload.id ?
          { ...flight, drones: [...flight.drones, action.payload.droneId] } :
          flight
        )
      };

    default:
      return state;
  }
}
