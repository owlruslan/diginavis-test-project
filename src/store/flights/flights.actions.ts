import {FlightPlan} from '../../models/flight-plan.model';
import {FlightStatus} from '../../models/flight-status.model';
import {AddFlightPlan} from '../../models/add-flight-plan.model';

export const LOAD_FLIGHTS = '[FLIGHTS] LOAD FLIGHTS'
export const LOAD_FLIGHTS_SUCCESS = '[FLIGHTS] LOAD FLIGHTS SUCCESS'
export const LOAD_FLIGHTS_FAILURE = '[FLIGHTS] LOAD FLIGHTS FAILURE'

export const ADD_FLIGHT = '[FLIGHTS] ADD FLIGHT'

export const REMOVE_FLIGHT = '[FLIGHTS] REMOVE FLIGHT'

export const SET_STATUS = '[FLIGHTS] SET STATUS'

export const ADD_DRONE_TO_PLAN = '[FLIGHTS] ADD DRONe TO PLAN'

// LOAD FLIGHTS
export function loadFlights(page?: number) {
  return {
    type: LOAD_FLIGHTS,
    payload: page
  }
}

export function loadFlightsSuccess(flights: FlightPlan[]) {
  return {
    type: LOAD_FLIGHTS_SUCCESS,
    payload: flights
  }
}

export function loadFlightsFailure(e: any) {
  return {
    type: LOAD_FLIGHTS_FAILURE,
    payload: e
  }
}

// ADD FLIGHT
export function addFlight(flight: AddFlightPlan) {
  return {
    type: ADD_FLIGHT,
    payload: flight
  }
}

// REMOVE FLIGHT
export function removeFlight(id: number) {
  return {
    type: REMOVE_FLIGHT,
    payload: id
  }
}

// SET STATUS
export function setStatus(id: number, status: FlightStatus) {
  return {
    type: SET_STATUS,
    payload: {id, status}
  }
}

// ADD DRONE TO PLAN
export function addDroneToPlan(id: number, droneId: number) {
  return {
    type: ADD_DRONE_TO_PLAN,
    payload: {id, droneId}
  }
}
