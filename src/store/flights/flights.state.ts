import {FlightPlan} from '../../models/flight-plan.model';

export interface State {
  entities: FlightPlan[];
}

export const initialState: State = {
  entities: [],
}
