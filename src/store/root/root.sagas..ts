import {dronesSagas} from '../drones/drones.sagas';
import {flightsSagas} from '../flights/flights.sagas';
import {all, fork} from 'redux-saga/effects';

export const sagas = [
  ...dronesSagas,
  ...flightsSagas
]

export default function* rootSaga() {
  yield all(sagas.map(saga => fork(saga)));
}
