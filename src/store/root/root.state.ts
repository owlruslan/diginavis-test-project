import {FlightsStoreState} from '../flights';
import {DronesStoreState} from '../drones';

export interface State {
  drones: DronesStoreState.State,
  flights: FlightsStoreState.State
}

export const initialState: State = {
  drones: DronesStoreState.initialState,
  flights: FlightsStoreState.initialState
};
