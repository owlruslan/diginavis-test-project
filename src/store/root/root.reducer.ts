import {combineReducers} from 'redux'
import {dronesReducer} from '../drones/drones.reducer';
import {flightsReducer} from '../flights/flights.reducer';

export const rootReducer = combineReducers({
  drones: dronesReducer,
  flights: flightsReducer
})
