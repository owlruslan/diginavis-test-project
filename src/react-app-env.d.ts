/// <reference types="react-scripts" />
import "@testing-library/jest-dom/extend-expect";
import {compose} from 'redux';

declare global {
  interface Window {
    __REDUX_DEVTOOLS_EXTENSION_COMPOSE__?: typeof compose;
  }
}

