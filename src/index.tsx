import React from 'react';
import {render} from 'react-dom';
import App from './App';
import {applyMiddleware, compose, createStore} from 'redux';
import {rootReducer} from './store/root/root.reducer';
import createSagaMiddleware from 'redux-saga';
import {Provider} from 'react-redux'
import rootSaga from './store/root/root.sagas.';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

const saga = createSagaMiddleware()

const store = createStore(
  rootReducer,
  composeEnhancers(
    applyMiddleware(
      saga
    )
  )
)

saga.run(rootSaga);

const app = (
  <Provider store={store}>
    <App/>
  </Provider>
)

render(app, document.getElementById('root'));
